package com.timefactor.service;

import com.timefactor.model.Appointment;
import com.timefactor.model.Branch;
import com.timefactor.model.User;

import java.util.ArrayList;
import java.util.Date;

public interface AppointmentService {
    ArrayList<Appointment> findBySpecAndDateAndBranchASC(User spec, Branch branch, Date date);
    void save(Appointment appointment);
}
