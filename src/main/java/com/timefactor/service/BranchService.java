package com.timefactor.service;

import com.timefactor.model.Branch;

import java.util.List;

public interface BranchService {
    List<Branch> getBranches();
    Branch findByAddress(String address);
    List<Branch> getSortingByAddressASC();
    List<Branch> getSortingByAddressDESC();
    List<Branch> findByAddressContaining(String address);
    List<Branch> getSortingByAddressAndContainingAddress(String address, String sort);
    void save(Branch branch);
    Branch getById(int id);
    void deleteBranch(Branch branch);
}
