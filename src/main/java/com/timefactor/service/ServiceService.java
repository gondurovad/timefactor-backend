package com.timefactor.service;

import com.timefactor.model.Service;

import java.util.List;

public interface ServiceService {
    List<Service> findAll();
    List<String> getBranchesByServiceId(int id);
    List<Service> retrieveByName(String name);
    List<Service> getSortingByPriceASC();
    List<Service> getSortingByPriceAndContainingName(String name, String sort);
    List<Service> getSortingByPriceDESC();
    void deleteService(Service service);
    Service getServiceById(int id);
    void save(Service service);
    List<Service> getServicesByBranch(Integer branchId);
    List<Service> getServicesBySpec(Integer specId);
}
