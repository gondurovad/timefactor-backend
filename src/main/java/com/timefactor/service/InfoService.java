package com.timefactor.service;

import com.timefactor.model.Info;

public interface InfoService {
    Info getById(int id);
    void save(Info info);
}
