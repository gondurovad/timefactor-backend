package com.timefactor.service;

import com.timefactor.model.Branch;
import com.timefactor.model.Timetable;
import com.timefactor.model.User;

import java.util.ArrayList;
import java.util.Date;

public interface TimetableService {
    void save(Timetable timetable);
    Timetable getById(int id);
    Timetable findBySpecAndBranchAndDate(int specId, int branchId, Date date);
    ArrayList<Timetable> findByBranchOfTimetableAndDateASC(int branchId, Date date);
}
