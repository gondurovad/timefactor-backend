package com.timefactor.service;

import com.timefactor.model.Branch;
import com.timefactor.model.Service;
import com.timefactor.model.User;

import java.util.List;

public interface UserService {
    User getUserById(int id);
    void save(User user);
    List<User> getSpecialists();
    List<User> getUsersWithSpecialistRequest();
    void deleteSpecialist(User user);
    void rejectRequest(User user);
    void acceptRequest(User user);
    List<Branch> getBranchesOfSpecialist(User specialist);
    boolean isServiceOfSpecialist(int userId, int specId);
    List<User> getSpecialistsOfBranch(int branchId);
    List<Service> getServicesOfSpecialist(User specialist);
}
