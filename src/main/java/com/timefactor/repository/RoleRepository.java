package com.timefactor.repository;

import com.timefactor.model.Role;
import com.timefactor.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    List<Role> findAll();
    Role findByName(Roles name);
}


