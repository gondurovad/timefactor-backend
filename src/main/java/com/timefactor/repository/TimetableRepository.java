package com.timefactor.repository;

import com.timefactor.model.Timetable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.Date;

public interface TimetableRepository extends JpaRepository<Timetable, Integer> {
    @Query(value="select * from timetable where specialist_id=:specId and branch_id=:branchId and start_datetime between date(:date) \n" +
            "\tand (date(:date)+1) limit 1", nativeQuery = true)
    Timetable findBySpecAndBranchAndDate(int specId, int branchId, Date date);

    @Query(value="select * from timetable where branch_id=:branchId and start_datetime between date(:date) \n" +
            "\tand (date(:date)+1) order by start_datetime", nativeQuery = true)
    ArrayList<Timetable> findByBranchOfTimetableAndDateASC(int branchId, Date date);
}
