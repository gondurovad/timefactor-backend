package com.timefactor.repository;

import com.timefactor.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BranchRepository extends JpaRepository<Branch, Integer> {
    Branch getBranchByAddress(String address);
    List<Branch> findAll ();

    Optional<Branch> findByAddress(String address);

    @Query(value="select * from branch order by address asc", nativeQuery = true)
    List<Branch> getSortingByAddressASC();

    @Query(value="select * from branch order by address desc", nativeQuery = true)
    List<Branch> getSortingByAddressDESC();

    List<Branch> findByAddressContainingIgnoreCase(String address);

    List<Branch> findByAddressContainingIgnoreCaseOrderByAddressAsc(String address);

    List<Branch> findByAddressContainingIgnoreCaseOrderByAddressDesc(String address);
}
