package com.timefactor.repository;

import com.timefactor.model.Branch;
import com.timefactor.model.Role;
import com.timefactor.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByTel(String tel);
    List<User> findByUserRole(Role role);
    List<User> findAllByUserRoleAndSpecialistRequest(Role role, boolean specialistRequest);

    @Query(value= "select branch_id from specialist_branch where user_id=:id", nativeQuery = true)
    List<Integer> findBranchesOfSpecialistId(Integer id);

    @Query(value="select count(*) from specialist_service where user_id=:userId and service_id=:serviceId", nativeQuery = true)
    int isServiceSpecialist(int userId, int serviceId);

    @Query(value = "select user_id from specialist_branch where branch_id=:branchId", nativeQuery = true)
    List<Integer> getSpecialistsOfBranch(int branchId);

    @Query(value = "select service_id from specialist_service where user_id=:specId", nativeQuery = true)
    List<Integer> getServicesOfSpecialist(int specId);
}

