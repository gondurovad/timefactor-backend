package com.timefactor.repository;

import com.timefactor.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Streamable;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service, Integer> {

    Streamable<Service> findByNameContainingIgnoreCase(String name);

    List<Service> findByNameContainingIgnoreCaseOrderByPriceAsc(String name);

    List<Service> findByNameContainingIgnoreCaseOrderByPriceDesc(String name);

    @Query(value="select * from service order by price asc", nativeQuery = true)
    List<Service> getSortingByPriceASC();

    @Query(value="select * from service order by price desc", nativeQuery = true)
    List<Service> getSortingByPriceDESC();

    @Query(value="select address from branch where branch_id in\n" +
            " (select branch_id from service_branch where service_id=:id)", nativeQuery = true)
    List<String> getBranchesByServiceId(@Param("id") int id);

    @Query(value="select service_id from service_branch where branch_id=:id", nativeQuery = true)
    List<Integer> getServicesIdByBranchId(@Param("id") int id);

    @Query(value="select service_id from specialist_service where user_id=:id", nativeQuery = true)
    List<Integer> getServicesIdBySpecId(@Param("id") int id);
}
