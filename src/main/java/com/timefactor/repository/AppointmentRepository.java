package com.timefactor.repository;

import com.timefactor.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.Date;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {
    @Query(value="select * from appointment where specialist_id=:specId and branch_id=:branchId and datetime between date(:date) \n" +
            "\tand (date(:date)+1) order by datetime", nativeQuery = true)
    ArrayList<Appointment> findBySpecAndDateAndBranchASC (Integer specId, Integer branchId, Date date);
}
