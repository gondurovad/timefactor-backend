package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class InfoDTO {
    private String name;
    private String description;
    private String site;
    private String photo;
}
