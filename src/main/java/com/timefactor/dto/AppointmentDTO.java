package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Builder(toBuilder = true)
@Data
public class AppointmentDTO {
    private int id;
    private int clientId;
    private int specId;
    private int branchId;
    private Date date;
    private List<Integer> servicesId;
}
