package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder(toBuilder = true)
@Data
public class SpecialistDTO {
    private int id;
    private String firstName;
    private String lastName;
    private String photo;
    private String description;
    private List<String> branches;
    private String position;
    private List<Integer> services;
}
