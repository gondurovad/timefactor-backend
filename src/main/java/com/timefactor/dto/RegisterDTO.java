package com.timefactor.dto;

import lombok.Data;

import java.util.Date;

@Data
public class RegisterDTO {
    private String tel;
    private String password;
    private String firstName;
    private String lastName;
    private Date dob;
    private String email;
    private boolean specialistRequest;
}
