package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class RequestDTO {
    private int id;
    private String tel;
    private String firstName;
    private String lastName;
    private String photo;
    private String email;
}
