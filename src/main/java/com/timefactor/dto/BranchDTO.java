package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class BranchDTO {
    private int id;
    private String address;
}
