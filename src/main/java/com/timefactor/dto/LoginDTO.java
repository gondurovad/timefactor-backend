package com.timefactor.dto;

import lombok.Data;

@Data
public class LoginDTO {
    private String tel;
    private String password;
}
