package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder(toBuilder = true)
@Data
public class ServiceDTO {
    private int id;
    private String name;
    private double price;
    private double duration;
    private List<String> addresses;
}
