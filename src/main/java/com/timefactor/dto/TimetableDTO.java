package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder(toBuilder = true)
@Data
public class TimetableDTO {
    private int id;
    private Date startDatetime;
    private Date endDatetime;
    private int specId;
    private int branchId;
}
