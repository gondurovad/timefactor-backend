package com.timefactor.dto;

import lombok.Data;

@Data
public class SortDTO {
    private String property;
    private String direction;
}
