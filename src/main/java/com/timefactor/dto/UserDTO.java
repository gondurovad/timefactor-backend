package com.timefactor.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder(toBuilder = true)
@Data
public class UserDTO {
    private String photo;
    private String firstName;
    private String lastName;
    private Date dob;
    private String email;
    private String description;
}
