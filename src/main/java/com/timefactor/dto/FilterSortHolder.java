package com.timefactor.dto;

import lombok.Data;

@Data
public class FilterSortHolder {
    private SortDTO sort;
    private FilterDTO filter;
}
