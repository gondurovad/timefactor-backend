package com.timefactor.dto;

import lombok.Data;

@Data
public class FilterDTO {
    private String property;
    private String operator;
    private String value;
}
