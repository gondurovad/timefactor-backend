package com.timefactor.controller;

import com.timefactor.dto.InfoDTO;
import com.timefactor.model.Info;
import com.timefactor.service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/info")
public class InfoController {
    @Autowired
    InfoService infoService;

    private final int infoId = 11;

    @PreAuthorize("hasAuthority('permit:all')")
    @GetMapping()
    public InfoDTO getInfo() {
        Info info = infoService.getById(infoId);
        InfoDTO infoDTO = InfoDTO.builder()
                .name(info.getName())
                .description(info.getDescription())
                .site(info.getSite())
                .photo(info.getPhoto())
                .build();
        return infoDTO;
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PatchMapping("/update")
    public void updateInfo(@RequestBody InfoDTO infoDTO) {
        Info info = infoService.getById(infoId);
        info.setName(infoDTO.getName());
        info.setDescription(infoDTO.getDescription());
        info.setSite(infoDTO.getSite());
        if (infoDTO.getPhoto() != null) {
            info.setPhoto(infoDTO.getPhoto());
        }
        System.out.println(infoDTO.getName());
        System.out.println(infoDTO.getPhoto());
        System.out.println(info.getPhoto());
        infoService.save(info);
    }
}
