package com.timefactor.controller;

import com.timefactor.dto.TimetableDTO;
import com.timefactor.model.Branch;
import com.timefactor.model.Timetable;
import com.timefactor.model.User;
import com.timefactor.service.TimetableService;
import com.timefactor.service.UserService;
import com.timefactor.serviceImpl.BranchServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/timetable")
public class TimetableController {
    @Autowired
    TimetableService timetableService;

    @Autowired
    UserService userService;

    @Autowired
    BranchServiceImpl branchService;

    @PreAuthorize("hasAuthority('admin:specialist:permission')")
    @PostMapping("/add")
    public void addTimetable(@RequestBody TimetableDTO timetableDTO) {
        Timetable timetable = new Timetable();
        timetable.setStartDatetime(timetableDTO.getStartDatetime());
        timetable.setEndDatetime(timetableDTO.getEndDatetime());
        User spec = userService.getUserById(timetableDTO.getSpecId());
        timetable.setSpecialistOfTimetable(spec);
        Branch branch = branchService.getById(timetableDTO.getBranchId());
        timetable.setBranchOfTimetable(branch);
        timetableService.save(timetable);
    }
}
