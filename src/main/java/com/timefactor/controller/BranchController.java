package com.timefactor.controller;

import com.timefactor.dto.*;
import com.timefactor.model.Branch;
import com.timefactor.model.Info;
import com.timefactor.service.BranchService;
import com.timefactor.service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/branches")
public class BranchController {
    @Autowired
    BranchService branchService;

    @Autowired
    InfoService infoService;

    private List<BranchDTO> toBranchDTO(List<Branch> branches) {
        List<BranchDTO> result = new ArrayList<BranchDTO>();
        for (Branch branch : branches) {
            BranchDTO branchDTO = BranchDTO.builder()
                    .id(branch.getId())
                    .address(branch.getAddress())
                    .build();
            result.add(branchDTO);
        }
        return result;
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('permit:all')")
    public List<BranchDTO> getAllBranches() {
        return toBranchDTO(branchService.getBranches());
    }

    @CrossOrigin
    @PreAuthorize("hasAuthority('admin:specialist:permission')")
    @GetMapping()
    public PagedListHolder getBranches(@RequestParam("pageNumber") int pageNumber) {
        List<BranchDTO> branches = toBranchDTO(branchService.getBranches());
        PagedListHolder branchesPage = new PagedListHolder(branches);
        branchesPage.setPage(pageNumber);
        branchesPage.setPageSize(4);
        return branchesPage;
    }

    @CrossOrigin
    @PreAuthorize("hasAuthority('admin:specialist:permission')")
    @PostMapping()
    public PagedListHolder getBranchesWithFilter(@RequestBody Optional<FilterSortHolder> holder,
                                                 @RequestParam("pageNumber") int pageNumber) {

        FilterDTO filterDTO = holder.get().getFilter();
        SortDTO sortDTO = holder.get().getSort();

        List<BranchDTO> result = new ArrayList<>();

        if (filterDTO.getValue().trim().isEmpty()
                && "default".equals(sortDTO.getDirection())) {
            return getBranches(pageNumber);
        } else if (filterDTO.getValue().trim().isEmpty() && !"default".equals(sortDTO.getDirection())) {
            List<Branch> branches;
            switch (sortDTO.getDirection().toUpperCase()) {
                case "ASC":
                    branches = branchService.getSortingByAddressASC();
                    result = toBranchDTO(branches);
                    break;
                case "DESC":
                    branches = branchService.getSortingByAddressDESC();
                    result = toBranchDTO(branches);
                    break;
                default:
                    System.out.println("sort by addresses in branches not recognized");
                    return getBranches(pageNumber);
            }
        } else if (!filterDTO.getValue().trim().isEmpty() && "default".equals(sortDTO.getDirection())) {
            List<Branch> branches = branchService.findByAddressContaining(filterDTO.getValue().trim());
            result = toBranchDTO(branches);
        } else {
            List<Branch> branches = branchService.
                    getSortingByAddressAndContainingAddress(filterDTO.getValue().trim(), sortDTO.getDirection());
            result = toBranchDTO(branches);
        }
        PagedListHolder branchesPage = new PagedListHolder(result);
        branchesPage.setPage(pageNumber);
        branchesPage.setPageSize(4);
        return branchesPage;
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('only:admin:permission')")
    public void createBranch(@RequestBody BranchDTO branchDTO) {
        if (branchDTO.getAddress().trim().isEmpty()) return;

        if (branchService.findByAddress(branchDTO.getAddress().trim())!=null) return;

        Branch branch = new Branch();
        branch.setAddress(branchDTO.getAddress().trim());

        Info info = infoService.getById(11);
        branch.setInfo(info);
        info.getBranches().add(branch);

        branchService.save(branch);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @DeleteMapping("/delete")
    public void deleteBranch(@RequestBody BranchDTO branchDTO) {
        Branch branch = branchService.getById(branchDTO.getId());
        infoService.getById(11).getBranches().remove(branch);
        if (branch != null) branchService.deleteBranch(branch);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PatchMapping("/update")
    public void updateBranch(@RequestBody BranchDTO branchDTO) {
        if (branchDTO.getAddress().trim().isEmpty()) return;
        Branch branch = branchService.getById(branchDTO.getId());
        branch.setAddress(branchDTO.getAddress());
        branchService.save(branch);
    }
}
