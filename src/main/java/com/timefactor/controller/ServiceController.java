package com.timefactor.controller;

import com.timefactor.dto.FilterDTO;
import com.timefactor.dto.FilterSortHolder;
import com.timefactor.dto.SortDTO;
import com.timefactor.dto.ServiceDTO;
import com.timefactor.model.Branch;
import com.timefactor.model.Service;
import com.timefactor.repository.BranchRepository;
import com.timefactor.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/services")
public class ServiceController {
    @Autowired
    ServiceService serviceService;

    @Autowired
    BranchRepository branchRepository;

    private List<ServiceDTO> toServiceDTO(List<Service> services) {
        List<ServiceDTO> result = new ArrayList<>();
        for (Service service : services) {
            ServiceDTO serviceDTO = ServiceDTO.builder()
                    .id(service.getId())
                    .name(service.getName())
                    .price(service.getPrice())
                    .duration(service.getDuration())
                    .build();

            List<String> branches = serviceService.getBranchesByServiceId(service.getId());
            serviceDTO.setAddresses(branches);

            result.add(serviceDTO);
        }

        return result;
    }

    @PreAuthorize("hasAuthority('admin:specialist:permission')")
    @GetMapping()
    public PagedListHolder getServices(@RequestParam("pageNumber") int pageNumber) {
        List<ServiceDTO> services = toServiceDTO(serviceService.findAll());
        PagedListHolder servicesPage = new PagedListHolder(services);
        servicesPage.setPage(pageNumber);
        servicesPage.setPageSize(4);
        return servicesPage;
    }

    @PreAuthorize("hasAuthority('admin:specialist:permission')")
    @PostMapping()
    public PagedListHolder getServicesWithFilter(@RequestBody Optional<FilterSortHolder> holder,
                                                 @RequestParam("pageNumber") int pageNumber) {

        FilterDTO filterDTO = holder.get().getFilter();
        SortDTO sortDTO = holder.get().getSort();

        List<ServiceDTO> result = new ArrayList<>();

        System.out.println(filterDTO);
        System.out.println(sortDTO);

        if (filterDTO.getValue().trim().isEmpty()
                && "default".equals(sortDTO.getDirection())) {
            return getServices(pageNumber);
        } else if (filterDTO.getValue().trim().isEmpty() && !"default".equals(sortDTO.getDirection())) {
            List<Service> services;
            switch (sortDTO.getDirection().toUpperCase()) {
                case "ASC":
                    services = serviceService.getSortingByPriceASC();
                    result = toServiceDTO(services);
                    break;
                case "DESC":
                    services = serviceService.getSortingByPriceDESC();
                    result = toServiceDTO(services);
                    break;
                default:
                    System.out.println("sort by price in services not recognized");
                    return getServices(pageNumber);
            }
        } else if (!filterDTO.getValue().trim().isEmpty() && "default".equals(sortDTO.getDirection())) {
            List<Service> services = serviceService.retrieveByName(filterDTO.getValue().trim());
            result = toServiceDTO(services);
        } else {
            List<Service> services = serviceService.
                    getSortingByPriceAndContainingName(filterDTO.getValue().trim(), sortDTO.getDirection());
            result = toServiceDTO(services);
        }
        PagedListHolder servicesPage = new PagedListHolder(result);
        servicesPage.setPage(pageNumber);
        servicesPage.setPageSize(4);
        return servicesPage;
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PostMapping("/add")
    public void createService(@RequestBody ServiceDTO serviceDTO) {
        if (serviceDTO.getName().trim().isEmpty()) return;

        Service service = new Service();
        service.setName(serviceDTO.getName());
        service.setPrice(serviceDTO.getPrice());
        service.setDuration(serviceDTO.getDuration());

        List<Branch> branches = new ArrayList<Branch>();
        for (String address : serviceDTO.getAddresses()) {
            Branch branch = branchRepository.getBranchByAddress(address);
            branches.add(branch);
        }

        service.setBranchesWithService(branches);

        if (!service.getBranchesWithService().isEmpty()) {
            for (Branch branch : service.getBranchesWithService()) {
                branch.getServicesOfBranch().add(service);
            }
        }

        serviceService.save(service);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @DeleteMapping("/delete")
    public void deleteService(@RequestBody ServiceDTO serviceDTO) {
        Service service = serviceService.getServiceById(serviceDTO.getId());
        if (service != null) serviceService.deleteService(service);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PatchMapping("/update")
    public void updateService(@RequestBody ServiceDTO serviceDTO) {
        if (serviceDTO.getName().trim().isEmpty()) return;
        Service service = serviceService.getServiceById(serviceDTO.getId());
        service.setName(serviceDTO.getName());
        service.setPrice(serviceDTO.getPrice());
        service.setDuration(serviceDTO.getDuration());
        serviceService.save(service);
    }

    @PreAuthorize("hasAuthority('only:client:permission')")
    @GetMapping("/branch/{branchId}")
    public List<ServiceDTO> getServicesByBranch(@PathVariable(value = "branchId") Integer branchId) {
        List<Service> services = serviceService.getServicesByBranch(branchId);
        if (!services.isEmpty()) return toServiceDTO(services);
        else return new ArrayList<>();
    }

    @PreAuthorize("hasAuthority('only:client:permission')")
    @GetMapping("/specialist/{specialistId}")
    public List<ServiceDTO> getServicesBySpec(@PathVariable(value = "specialistId") Integer specId) {
        List<Service> services = serviceService.getServicesBySpec(specId);
        if (!services.isEmpty()) return toServiceDTO(services);
        else return new ArrayList<>();
    }
}
