package com.timefactor.controller;

import com.timefactor.dto.RequestDTO;
import com.timefactor.dto.SpecialistDTO;
import com.timefactor.dto.UserDTO;
import com.timefactor.model.Branch;
import com.timefactor.model.Service;
import com.timefactor.model.User;
import com.timefactor.service.BranchService;
import com.timefactor.service.ServiceService;
import com.timefactor.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/users")
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    BranchService branchService;

    @Autowired
    ServiceService serviceService;

    private List<SpecialistDTO> toSpecialistDTOList(List<User> users) {
        List<SpecialistDTO> result = new ArrayList<>();
        for (User user : users) {
            SpecialistDTO specialistDTO = SpecialistDTO.builder()
                    .id(user.getId())
                    .firstName(user.getFirst_name())
                    .lastName(user.getLast_name())
                    .photo(user.getPhoto())
                    .description(user.getDescription())
                    .position(user.getPosition())
                    .build();

            List<Branch> branches = userService.getBranchesOfSpecialist(user);
            List<String> branchesDTO = new ArrayList<String>();
            for (Branch branch: branches)
                branchesDTO.add(branch.getAddress());
            specialistDTO.setBranches(branchesDTO);

            List<Service> services = userService.getServicesOfSpecialist(user);
            List<Integer> servicesId = new ArrayList<Integer>();
            for (Service service: services)
                servicesId.add(service.getId());
            specialistDTO.setServices(servicesId);

            result.add(specialistDTO);
        }
        return result;
    }

    @PreAuthorize("hasAuthority('permit:all')")
    @GetMapping("/info/{id}")
    public UserDTO getUserDTOById(@PathVariable(value = "id") Integer id) {
        User user = userService.getUserById(id);
        UserDTO userDTO = UserDTO.builder()
                .photo(user.getPhoto())
                .firstName(user.getFirst_name())
                .lastName(user.getLast_name())
                .dob(user.getDob())
                .email(user.getEmail())
                .description(user.getDescription())
                .build();
        return userDTO;
    }

    @PreAuthorize("hasAuthority('permit:all')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserHeaderInfoById(@PathVariable(value = "id") Integer id) {
        User user = userService.getUserById(id);
        Map<Object, Object> response = new HashMap<>();
        response.put("photo", user.getPhoto());
        response.put("firstName", user.getFirst_name());
        response.put("lastName", user.getLast_name());
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('permit:all')")
    @PatchMapping("/update/{id}")
    public void updateUser(@RequestBody UserDTO userDTO, @PathVariable(value = "id") Integer id) {
        User user = userService.getUserById(id);
        if (userDTO.getPhoto() != null)
            user.setPhoto(userDTO.getPhoto());
        user.setFirst_name(userDTO.getFirstName());
        user.setLast_name(userDTO.getLastName());
        user.setDob(userDTO.getDob());
        user.setEmail(userDTO.getEmail());
        user.setDescription(userDTO.getDescription());
        userService.save(user);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @GetMapping("/specialists")
    public List<SpecialistDTO> getSpecialists() {
        return toSpecialistDTOList(userService.getSpecialists());
    }

    @PreAuthorize("hasAuthority('only:client:permission')")
    @GetMapping("/specialists/{branchId}")
    public List<SpecialistDTO> getSpecialistsOfBranch(@PathVariable(value = "branchId") Integer branchId) {
        return toSpecialistDTOList(userService.getSpecialistsOfBranch(branchId));
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @GetMapping("/requests")
    public List<RequestDTO> getUsersWithSpecialistRequest() {
        List<User> users = userService.getUsersWithSpecialistRequest();
        List<RequestDTO> result = new ArrayList<>(); //RequestDTO с id и tel!!!
        for (User user : users) {
            RequestDTO requestDTO = RequestDTO.builder()
                    .photo(user.getPhoto())
                    .firstName(user.getFirst_name())
                    .lastName(user.getLast_name())
                    .id(user.getId())
                    .tel(user.getTel())
                    .email(user.getEmail())
                    .build();
            result.add(requestDTO);
        }
        return result;
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PatchMapping("/specialist/update")
    public void updateSpecialist(@RequestBody SpecialistDTO specialistDTO) {
        User user = userService.getUserById(specialistDTO.getId());
        if (specialistDTO.getPosition() != null)
            user.setPosition(specialistDTO.getPosition());
        if (specialistDTO.getBranches() != null) {
            List<String> branchesNames = specialistDTO.getBranches();
            List<Branch> branches = new ArrayList<Branch>();
            for (String branchName : branchesNames)
                branches.add(branchService.findByAddress(branchName));
            user.setBranchesOfSpecialist(branches);
        }
        if (specialistDTO.getServices() != null) {
            List<Integer> servicesId = specialistDTO.getServices();
            List<Service> services = new ArrayList<Service>();
            for (Integer id : servicesId)
                services.add(serviceService.getServiceById(id));
            user.setServicesOfSpecialist(services);
        }
        userService.save(user);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PostMapping("/specialist/delete")
    public void deleteSpecialist(@RequestBody SpecialistDTO specialistDTO) {
        User user = userService.getUserById(specialistDTO.getId());
        userService.deleteSpecialist(user);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PostMapping("/request/reject")
    public void rejectRequest(@RequestBody RequestDTO requestDTO) {
        User user = userService.getUserById(requestDTO.getId());
        userService.rejectRequest(user);
    }

    @PreAuthorize("hasAuthority('only:admin:permission')")
    @PostMapping("/request/accept")
    public void acceptRequest(@RequestBody RequestDTO requestDTO) {
        User user = userService.getUserById(requestDTO.getId());
        userService.acceptRequest(user);
    }
}
