package com.timefactor.controller;

import com.timefactor.dto.AppointmentDTO;
import com.timefactor.dto.SpecialistDTO;
import com.timefactor.model.*;
import com.timefactor.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/appointment")
public class AppointmentController {
    @Autowired
    AppointmentService appointmentService;

    @Autowired
    TimetableService timetableService;

    @Autowired
    ServiceService serviceService;

    @Autowired
    UserService userService;

    @Autowired
    BranchService branchService;

    private List<Date> findSlots (Timetable timetable, User spec, Branch branch, Date date, int duration) {
        Date start = timetable.getStartDatetime();
        Date end = timetable.getEndDatetime();

        ArrayList<Date> initialSegment = new ArrayList<>();
        initialSegment.add(start);
        initialSegment.add(end);
        System.out.println("spec working hours: "+initialSegment);
        System.out.println("duration: "+duration);

        HashMap<Integer, ArrayList<Date>> timeSlots = new HashMap<>();
        timeSlots.put(0, initialSegment);

        ArrayList<Appointment> appointments = appointmentService.findBySpecAndDateAndBranchASC(spec, branch, date);

        // определим свободные слоты
        for (Appointment appointment : appointments) {
            Date startTime = appointment.getDatetime();
            Date endTime = appointment.getEndDateTime();
            for (int i = 0; timeSlots.get(i) != null; i++) {
                Integer key = i;
                ArrayList<Date> value = timeSlots.get(i);
                //содержится ли startTime-endTime внутри value
                //если да, то поделить value на два
                if (!(startTime.before(value.get(0)) || endTime.after(value.get(1)))) {
                    ArrayList<Date> firstSegment = new ArrayList<>();
                    ArrayList<Date> secondSegment = new ArrayList<>();
                    firstSegment.add(value.get(0));
                    firstSegment.add(startTime);
                    secondSegment.add(endTime);
                    secondSegment.add(value.get(1));
                    timeSlots.put(key, firstSegment);
                    timeSlots.put(key + 1, secondSegment);
                    break;
                }
            }
        }
        System.out.println("finish slots: " + timeSlots);

        // найдем слоты, подходящие по длительности
        ArrayList<Date> result = new ArrayList<Date>();
        for (int i=0; i<timeSlots.size(); i++) {
            ArrayList<Date> slot = timeSlots.get(i);
            long slotDuration = TimeUnit.MINUTES.convert(slot.get(1).getTime()-slot.get(0).getTime(), TimeUnit.MILLISECONDS);
            System.out.println("slot duration: "+slotDuration);
            if (duration<=slotDuration) {
                int count = (int)slotDuration/duration;
                if (count<2) result.add(slot.get(0));
                else
                    for (int j=0; j<count; j++) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(slot.get(0));
                        cal.add(Calendar.MINUTE, j*duration);
                        result.add(cal.getTime());
                    }
            }
        }
        System.out.println("choice: "+result);
        return result;
    }

    private int countDuration(List<Integer> servicesId) {
        int duration = 10; //промежуток между записями
        for (Integer id: servicesId) {
            Service service = serviceService.getServiceById(id);
            if (service!=null) duration+=service.getDuration();
        }
        if (duration%30 != 0)
            duration = duration%60 > 30 ? (duration/60)*60+60 : (duration/60)*60+30;
        return duration;
    }

    private boolean specialistProvidesServices(int specId, List<Integer> servicesId) {
        boolean flag = false;
        for (Integer id: servicesId)
            if (!userService.isServiceOfSpecialist(specId, id)) flag=true;
        if (flag==true) return false;
        else return true;
    }

    private List<Service> toServices (List<Integer> servicesId) {
        ArrayList<Service> services = new ArrayList<>();
        servicesId.forEach(id-> {
            Service service = serviceService.getServiceById(id);
            if (service!=null) services.add(service);
        });
        return services;
    }

    @PreAuthorize("hasAuthority('only:client:permission')")
    @PostMapping("/slots")
    public Map<Date, Integer> getSlots(@RequestBody AppointmentDTO appointmentDTO) {
        Integer clientId = appointmentDTO.getClientId();
        Integer specId = appointmentDTO.getSpecId();
        Integer branchId = appointmentDTO.getBranchId();
        Date date = appointmentDTO.getDate();
        List<Integer> servicesId = appointmentDTO.getServicesId();
        System.out.println(specId+"  "+clientId+"  "+branchId+"  "+date);
        if (clientId!=0 && branchId!=0 && date!=null && !servicesId.isEmpty()) {
            int duration = countDuration(servicesId);
            Branch branch = branchService.getById(branchId);
            if (specId != 0) {
                if(!specialistProvidesServices(specId, servicesId)) return new HashMap<>();
                User spec = userService.getUserById(specId);
                Timetable timetable = timetableService.findBySpecAndBranchAndDate(specId, branchId, date);
                if (timetable==null) return new HashMap<>();
                else {
                    List<Date> slots = findSlots(timetable, spec, branch, date, duration);
                    Map<Date, Integer> response = new HashMap<>();
                    for (Date slot: slots) response.put(slot, specId);
                    return response;
                }
            } else {
                ArrayList<Timetable> timetables = timetableService.findByBranchOfTimetableAndDateASC(branchId, date);
                Map<Date, Integer> response = new TreeMap<Date, Integer>();
                System.out.println("services id: "+servicesId);
                for (Timetable timetable: timetables) {
                    User spec = timetable.getSpecialistOfTimetable();
                    int specialistId = spec.getId();
                    if(!specialistProvidesServices(specialistId, servicesId)) continue;
                    List<Date> slots = findSlots(timetable, spec, branch, date, duration);
                    for (Date slot: slots) response.put(slot, specialistId);
                }
                System.out.println(response);
                return response;
            }
        }
        return new HashMap<>();
    }

    @PreAuthorize("hasAuthority('only:client:permission')")
    @PostMapping("/add")
    public ResponseEntity<?> addAppointment(@RequestBody AppointmentDTO appointmentDTO) {
        Integer clientId = appointmentDTO.getClientId();
        Integer specId = appointmentDTO.getSpecId();
        Integer branchId = appointmentDTO.getBranchId();
        Date date = appointmentDTO.getDate();
        List<Integer> servicesId = appointmentDTO.getServicesId();
        if (clientId!=0 && specId!=0 && branchId!=0 && date!=null && !servicesId.isEmpty()) {
            if(!specialistProvidesServices(specId, servicesId))
                return new ResponseEntity<>("The selected services are not supported by the selected specialist", HttpStatus.CONFLICT);

            User spec = userService.getUserById(appointmentDTO.getSpecId());
            User client = userService.getUserById(appointmentDTO.getClientId());
            Branch branch = branchService.getById(appointmentDTO.getBranchId());
            List<Service> services = toServices(appointmentDTO.getServicesId());

            int duration = countDuration(appointmentDTO.getServicesId());
            Date start = appointmentDTO.getDate();
            Calendar cal = Calendar.getInstance();
            cal.setTime(start);
            cal.add(Calendar.MINUTE, duration);
            Date end = cal.getTime();

            Appointment appointment = new Appointment();
            appointment.setDatetime(start);
            appointment.setEndDateTime(end);
            appointment.setClientAppointment(client);
            appointment.setSpecialistAppointment(spec);
            appointment.setBranchAppointment(branch);
            appointment.setServices(services);

            appointmentService.save(appointment);
            return new ResponseEntity<>(HttpStatus.OK);
        } else return new ResponseEntity<>("Not all fields are filled", HttpStatus.BAD_REQUEST);
    }
}
