package com.timefactor.controller;

import com.timefactor.dto.LoginDTO;
import com.timefactor.dto.RegisterDTO;
import com.timefactor.model.Status;
import com.timefactor.model.User;
import com.timefactor.repository.RoleRepository;
import com.timefactor.repository.UserRepository;
import com.timefactor.security.JwtAuthenticationException;
import com.timefactor.security.JwtTokenProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/timefactor/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private JwtTokenProvider jwtTokenProvider;

    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login (@RequestBody LoginDTO request) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getTel(), request.getPassword()));
            User user = userRepository.findByTel(request.getTel()).orElseThrow(()-> new UsernameNotFoundException("User doesn't exists"));
            String token = jwtTokenProvider.createToken(request.getTel(), user.getUserRole().getName().name());
            Map<Object, Object> response = new HashMap<>();
            response.put("tel", request.getTel());
            response.put("userRole", user.getUserRole().getName().name());
            response.put("userId", user.getId());
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (JwtAuthenticationException e) {
            return new ResponseEntity<>("Invalid tel or password", HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/register")
    public ResponseEntity register (@RequestBody RegisterDTO registerDTO) {
        if (userRepository.findByTel(registerDTO.getTel()).orElse(null) == null) {
            User user = new User();
            user.setTel(registerDTO.getTel());
            user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
            user.setUserRole(roleRepository.getById(79));
            user.setStatus(Status.ACTIVE);
            if (registerDTO.isSpecialistRequest()) user.setSpecialistRequest(true);
            else user.setSpecialistRequest(false);
            user.setFirst_name(registerDTO.getFirstName());
            user.setLast_name(registerDTO.getLastName());
            user.setDob(registerDTO.getDob());
            user.setEmail(registerDTO.getEmail());
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}