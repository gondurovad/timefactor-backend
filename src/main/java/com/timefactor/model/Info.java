package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="info")
@Data
@NoArgsConstructor
public class Info {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="info_id")
    private int id;

    private String name;

    private String description;

    private String site;

    private String photo;

    @OneToMany(mappedBy = "info", fetch = FetchType.EAGER)
    private List<Branch> branches;
}

