package com.timefactor.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Roles {
    ADMIN(Set.of(Permission.ONLY_ADMIN_PERMISSION, Permission.PERMIT_ALL, Permission.ADMIN_SPECIALIST_PERMISSION)),
    SPECIALIST(Set.of(Permission.ONLY_SPECIALIST_PERMISSION, Permission.PERMIT_ALL, Permission.ADMIN_SPECIALIST_PERMISSION)),
    CLIENT(Set.of(Permission.ONLY_CLIENT_PERMISSION, Permission.PERMIT_ALL));

    private final Set<Permission> permissions;

    Roles(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
    }
}
