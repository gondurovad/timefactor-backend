package com.timefactor.model;

public enum Status {
    ACTIVE, BANNED
}
