package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="service")
@Data
@NoArgsConstructor
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="service_id")
    private int id;

    private String name;

    private double price;

    private double duration;

    @ManyToMany(mappedBy = "servicesOfBranch")
    private List<Branch> branchesWithService;

    @PreRemove
    private void removeServiceInServicesOfBranch() {
        for (Branch branch: branchesWithService) {
            branch.getServicesOfBranch().remove(this);
        }
        //for (User specialist: specialistsAbleDoService)
        //logic for appointments
    }

    @ManyToMany(mappedBy = "servicesOfSpecialist")
    private List<User> specialistsAbleDoService;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "service_appointment",
            joinColumns = @JoinColumn(name = "service_id"),
            inverseJoinColumns = @JoinColumn(name = "appointment_id")
    )
    private List<Appointment> appointmentsForService;
}

