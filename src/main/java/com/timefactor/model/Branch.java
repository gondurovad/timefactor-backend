package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="branch")
@Data
@NoArgsConstructor
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="branch_id")
    private int id;

    private String address;

    @OneToMany(mappedBy = "branchAppointment", fetch = FetchType.EAGER)
    private List<Appointment> branchAppointmentsList;

    @OneToMany(mappedBy = "branchOfTimetable", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Timetable> branchTimetableList;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "info_id")
    private Info info;

    @ManyToMany(mappedBy = "branchesOfSpecialist")
    private List<User> specialistsWorkingInBranch;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "service_branch",
            joinColumns = @JoinColumn(name = "branch_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private List<Service> servicesOfBranch;

    @PreRemove
    private void removeBranchesFromBranchesWithService() {
        for (Service service: servicesOfBranch) {
            service.getBranchesWithService().remove(this);
        }
        //
    }
}

