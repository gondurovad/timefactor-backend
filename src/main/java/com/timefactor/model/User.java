package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="users")
@Data
@NoArgsConstructor
public class User extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="user_id")
    private int id;

    private String password;

    private Date dob;

    private String first_name;

    private String last_name;

    private String tel;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "role_id")
    private Role userRole;

    private String email;

    private String photo;

    private String description;

    private String position;

    @Enumerated(value=EnumType.STRING)
    private Status status;

    @Column(name="specialist_request")
    private boolean specialistRequest;

    @OneToMany(mappedBy = "clientAppointment", fetch = FetchType.EAGER)
    private List<Appointment> clientAppointmentsList;

    @OneToMany(mappedBy = "specialistAppointment", fetch = FetchType.EAGER)
    private List<Appointment> specialistAppointmentsList;

    @OneToMany(mappedBy = "specialistOfTimetable", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Timetable> specialistTimetableList;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "specialist_branch",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "branch_id")
    )
    private List<Branch> branchesOfSpecialist;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "specialist_service",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private List<Service> servicesOfSpecialist;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", dob=" + dob +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", tel=" + tel +
                ", userRole_name=" + userRole.getName() +
                ", email='" + email + '\'' +
                ", photo=" + photo +
                ", description='" + description + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}


