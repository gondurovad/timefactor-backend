package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="timetable")
@Data
@NoArgsConstructor
public class Timetable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="timetable_id")
    private int id;

    @Column(name="start_datetime")
    private Date startDatetime;

    @Column(name="end_datetime")
    private Date endDatetime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "specialist_id")
    private User specialistOfTimetable;

    @ManyToOne(optional = false)
    @JoinColumn(name = "branch_id")
    private Branch branchOfTimetable;
}

