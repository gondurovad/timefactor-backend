package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="appointment")
@Data
@NoArgsConstructor
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="appointment_id")
    private int id;

    private Date datetime;

    private Date endDateTime;

    @ManyToMany(mappedBy = "appointmentsForService")
    private List<Service> services;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id")
    private User clientAppointment;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "specialist_id")
    private User specialistAppointment;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "branch_id")
    private Branch branchAppointment;
}


