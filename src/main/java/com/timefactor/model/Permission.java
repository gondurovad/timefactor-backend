package com.timefactor.model;

public enum Permission {
    ONLY_ADMIN_PERMISSION("only:admin:permission"),
    ONLY_SPECIALIST_PERMISSION("only:specialist:permission"),
    ONLY_CLIENT_PERMISSION("only:client:permission"),
    ADMIN_SPECIALIST_PERMISSION("admin:specialist:permission"),
    PERMIT_ALL("permit:all");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
