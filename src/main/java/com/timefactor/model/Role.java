package com.timefactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="role")
@Data
@NoArgsConstructor
public class Role extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="role_id")
    private int id;

    //private String name;
    @Enumerated(value=EnumType.STRING)
    private Roles name;

    @OneToMany(mappedBy = "userRole", fetch = FetchType.EAGER)
    private List<User> usersByRoleList;
}


