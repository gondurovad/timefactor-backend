package com.timefactor.serviceImpl;

import com.timefactor.model.Branch;
import com.timefactor.model.Role;
import com.timefactor.model.Roles;
import com.timefactor.model.User;
import com.timefactor.repository.BranchRepository;
import com.timefactor.repository.RoleRepository;
import com.timefactor.repository.ServiceRepository;
import com.timefactor.repository.UserRepository;
import com.timefactor.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Override
    public User getUserById(int id) {
        return repository.getById(id);
    }

    @Override
    public void save(User user) {
        repository.save(user);
    }

    @Override
    public List<User> getSpecialists() {
        Role role = roleRepository.findByName(Roles.SPECIALIST);
        List<User> users = repository.findByUserRole(role);
        return users;
    }

    @Override
    public List<User> getUsersWithSpecialistRequest() {
        Role role = roleRepository.findByName(Roles.CLIENT);
        List<User> users = repository.findAllByUserRoleAndSpecialistRequest(role, true);
        return users;
    }

    @Override
    public void deleteSpecialist(User user) {
        Role role = roleRepository.findByName(Roles.CLIENT);
        user.setUserRole(role);
        if (user.getBranchesOfSpecialist() != null)
            user.setBranchesOfSpecialist(null);
        if (user.getServicesOfSpecialist() != null)
            user.setServicesOfSpecialist(null);
        user.setPosition(null);
        user.setSpecialistRequest(false);
        repository.save(user);
    }

    @Override
    public void rejectRequest(User user) {
        user.setSpecialistRequest(false);
        repository.save(user);
    }

    @Override
    public void acceptRequest(User user) {
        Role role = roleRepository.findByName(Roles.SPECIALIST);
        user.setUserRole(role);
        user.setSpecialistRequest(false);
        repository.save(user);
    }

    @Override
    public List<Branch> getBranchesOfSpecialist(User specialist) {
        List<Integer> branchesId = repository.findBranchesOfSpecialistId(specialist.getId());
        List<Branch> branches = new ArrayList<>();
        for (Integer id: branchesId)
            branches.add(branchRepository.getById(id));
        return branches;
    }

    @Override
    public boolean isServiceOfSpecialist(int userId, int serviceId) {
        int count = repository.isServiceSpecialist(userId, serviceId);
        if (count==0) return false;
        else return true;
    }

    @Override
    public List<User> getSpecialistsOfBranch(int branchId) {
        List<Integer> specIds = repository.getSpecialistsOfBranch(branchId);
        List<User> specialists = new ArrayList<>();
        for (Integer id: specIds) {
            User spec = repository.getById(id);
            specialists.add(spec);
        }
        return specialists;
    }

    @Override
    public List<com.timefactor.model.Service> getServicesOfSpecialist(User specialist) {
        List<Integer> servicesId = repository.getServicesOfSpecialist(specialist.getId());
        List<com.timefactor.model.Service> services = new ArrayList<>();
        for (Integer id: servicesId) {
            services.add(serviceRepository.getById(id));
        }
        return services;
    }
}

