package com.timefactor.serviceImpl;

import com.timefactor.model.Info;
import com.timefactor.repository.InfoRepository;
import com.timefactor.service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InfoServiceImpl implements InfoService {
    @Autowired
    InfoRepository infoRepository;

    @Override
    public Info getById (int id) {
        return infoRepository.getById(id);
    }

    @Override
    public void save(Info info) {
        infoRepository.save(info);
    }
}
