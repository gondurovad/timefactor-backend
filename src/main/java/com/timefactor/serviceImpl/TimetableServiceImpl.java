package com.timefactor.serviceImpl;

import com.timefactor.model.Branch;
import com.timefactor.model.Timetable;
import com.timefactor.model.User;
import com.timefactor.repository.TimetableRepository;
import com.timefactor.service.TimetableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;

@Service
@Transactional
public class TimetableServiceImpl implements TimetableService {
    @Autowired
    private TimetableRepository timetableRepository;

    @Override
    public void save(Timetable timetable) {
        timetableRepository.save(timetable);
    }

    @Override
    public Timetable getById(int id) {
        Timetable timetable = timetableRepository.getById(id);
        timetable.getStartDatetime();
        return timetable;
    }

    @Override
    public Timetable findBySpecAndBranchAndDate(int specId, int branchId, Date date) {
        return timetableRepository.findBySpecAndBranchAndDate(specId, branchId, date);
    }

    @Override
    public ArrayList<Timetable> findByBranchOfTimetableAndDateASC(int branchId, Date date) {
        return timetableRepository.findByBranchOfTimetableAndDateASC(branchId, date);
    }
}
