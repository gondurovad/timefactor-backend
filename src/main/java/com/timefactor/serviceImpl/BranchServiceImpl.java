package com.timefactor.serviceImpl;

import com.timefactor.model.Branch;
import com.timefactor.repository.BranchRepository;
import com.timefactor.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchServiceImpl implements BranchService {
    @Autowired
    private BranchRepository branchRepository;

    @Override
    public List<Branch> getBranches() {
        return branchRepository.findAll();
    }

    @Override
    public Branch findByAddress(String address) {
        return branchRepository.findByAddress(address).orElse(null);
    }

    @Override
    public List<Branch> getSortingByAddressASC() {
        return branchRepository.getSortingByAddressASC();
    }

    @Override
    public List<Branch> getSortingByAddressDESC() {
        return branchRepository.getSortingByAddressDESC();
    }

    @Override
    public List<Branch> findByAddressContaining(String address) {
        return branchRepository.findByAddressContainingIgnoreCase(address);
    }

    @Override
    public List<Branch> getSortingByAddressAndContainingAddress(String address, String sort) {
        if ("ASC".equals(sort.toUpperCase()))
            return branchRepository.findByAddressContainingIgnoreCaseOrderByAddressAsc(address);
        else return branchRepository.findByAddressContainingIgnoreCaseOrderByAddressDesc(address);
    }

    @Override
    public void save(Branch branch) {
        branchRepository.save(branch);
    }

    @Override
    public Branch getById(int id) {
        return branchRepository.getById(id);
    }

    @Override
    public void deleteBranch(Branch branch) {
        branchRepository.delete(branch);
    }
}
