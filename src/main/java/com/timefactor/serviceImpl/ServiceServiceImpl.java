package com.timefactor.serviceImpl;

import com.timefactor.model.Service;
import com.timefactor.repository.ServiceRepository;
import com.timefactor.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;

    @Override
    public List<Service> findAll () {
        return serviceRepository.findAll();
    }

    @Override
    public List<String> getBranchesByServiceId (int id) {
        return serviceRepository.getBranchesByServiceId(id);
    }

    @Override
    public List<Service> retrieveByName(String name) {
        Streamable<Service> services = serviceRepository.findByNameContainingIgnoreCase(name);
        List<Service> result = services.toList();
        return result;
    }

    @Override
    public List<Service> getSortingByPriceASC() {
        return serviceRepository.getSortingByPriceASC();
    }

    @Override
    public List<Service> getSortingByPriceAndContainingName(String name, String sort) {
        if ("ASC".equals(sort.toUpperCase()))
        return serviceRepository.findByNameContainingIgnoreCaseOrderByPriceAsc(name);
        else return serviceRepository.findByNameContainingIgnoreCaseOrderByPriceDesc(name);
    }

    @Override
    public List<Service> getSortingByPriceDESC() {
        return serviceRepository.getSortingByPriceDESC();
    }

    @Override
    public void deleteService(Service service) {
        serviceRepository.delete(service);
    }

    @Override
    public Service getServiceById (int id) {
        return serviceRepository.getById(id);
    }

    @Override
    public void save (Service service) {
        serviceRepository.save(service);
    }

    @Override
    public List<Service> getServicesByBranch(Integer branchId) {
        List<Integer> servicesId = serviceRepository.getServicesIdByBranchId(branchId);
        List<Service> services = new ArrayList<>();
        for (Integer id: servicesId)
            services.add(serviceRepository.getById(id));
        return services;
    }

    @Override
    public List<Service> getServicesBySpec(Integer specId) {
        List<Integer> servicesId = serviceRepository.getServicesIdBySpecId(specId);
        List<Service> services = new ArrayList<>();
        for (Integer id: servicesId)
            services.add(serviceRepository.getById(id));
        return services;
    }
}
