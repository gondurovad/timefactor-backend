package com.timefactor.serviceImpl;

import com.timefactor.model.Appointment;
import com.timefactor.model.Branch;
import com.timefactor.model.User;
import com.timefactor.repository.AppointmentRepository;
import com.timefactor.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;

@Service
public class AppointmentServiceImpl implements AppointmentService {
    @Autowired
    AppointmentRepository appointmentRepository;

    @Transactional
    @Override
    public ArrayList<Appointment> findBySpecAndDateAndBranchASC (User spec, Branch branch, Date date) {
        return appointmentRepository.findBySpecAndDateAndBranchASC(spec.getId(), branch.getId(), date);
    }

    @Override
    public void save(Appointment appointment) {
        appointmentRepository.save(appointment);
    }
}
